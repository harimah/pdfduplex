#!/usr/bin/python3
# -*- coding: utf-8 -*-

import PyPDF2 as pdf
import sys
import os


def pdfduplex (VS, RS):
    output= pdf.PdfWriter()

    # Checken ob Anzahl der Seiten übereinstimmt
    AnzahlVorderSeiten = len(VS.pages)
    AnzahlRueckSeiten = len(RS.pages)
    if AnzahlVorderSeiten != AnzahlRueckSeiten:
        return -1, None

    else:
        AnzahlSeiten = AnzahlVorderSeiten

    #Seiten umsortieren
    for i in range(0, AnzahlSeiten):
        Seite = VS.pages[i]
        output.add_page(Seite)
        Seite = RS.pages[AnzahlSeiten-1-i]
        output.add_page(Seite)
    return 0, output

if __name__ =="__main__":
    if len(sys.argv) != 3:
        print ('Es müssen Vorder- und Rückseiten übergeben werden, breche ab...')
        sys.exit(1)

    # Existenz der Eingabedateien checken und öffnen
    try:
        VorderSeite = pdf.PdfReader(open(sys.argv[1], 'rb'))
    except:
        print (sys.argv[1], 'existiert nicht oder ist kein PDF, breche ab...')
        sys.exit(1)

    try:
        RueckSeite = pdf.PdfReader(open(sys.argv[2], 'rb'))
    except:
        print (sys.argv[2], 'existiert nicht oder ist kein PDF, breche ab...')
        sys.exit(1)

    output_code, output = pdfduplex(VS=VorderSeite, RS=RueckSeite)
    if output_code == 0:
        with open('new.pdf', 'wb') as f:
            output.write(f)
    elif output_code == -1:
        print('Anzahl Seiten Vorder- und Rückseiten unterschiedlich, breche ab...')
        sys.exit(1)


# Eingangsdateien löschen
# os.remove(sys.argv[1])
# os.remove(sys.argv[2])

