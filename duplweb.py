#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 harald <harald@rbserver>
#

import sys
import os
import PyPDF2 as pdf
from flask import Flask, render_template, request, flash, redirect, send_file

# Import der Funktion zum Zusammensetzen (eigenes script dupl.py im selben Ordner)
from dupl import pdfduplex

app = Flask(__name__)

# os.getcwd() funktioniert mit flask nicht !
WORKING_DIR = app.root_path
OUTPUT_FILE_NAME = 'merged.pdf'
OUTPUT_FILE_FULL_PATH = os.path.join(WORKING_DIR,OUTPUT_FILE_NAME)

# Zum Identifizieren der Session, notwendig für flash()
# Zufälliger Byte-Folge wird mit os.urandom() erzeugt
app.secret_key = os.urandom(12)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        file_vs = request.files['vs']
        file_rs = request.files['rs']

        if file_vs.filename != '' and file_rs.filename != '':
            try:
                pdf_file_vs = pdf.PdfFileReader(file_vs)
            except:
                flash('"%s" ist kein PDF' % file_vs.filename)
                return redirect(request.url)
            try:
                pdf_file_rs = pdf.PdfFileReader(file_rs)
            except:
                flash('"%s" ist kein PDF' % file_rs.filename)
                return redirect(request.url)
            
            return_code, pdf_file_merged = pdfduplex(pdf_file_vs, pdf_file_rs)
            if return_code == 0:
                with open(OUTPUT_FILE_FULL_PATH, 'wb') as f:
                    pdf_file_merged.write(f)
                return send_file(OUTPUT_FILE_FULL_PATH)
            elif return_code == -1:
                flash('Vorder- und Rückseite müssen gleiche Anzahl von Seiten haben')
                return redirect(request.url)
        else:
            flash ('Vorder- und Rückseite müssen hochgeladen werden')
            return redirect(request.url)
    else:
        return render_template('upload.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000, debug=True)
