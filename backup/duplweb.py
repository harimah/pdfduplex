#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 harald <harald@rbserver>
#

import sys
import os
import PyPDF2 as pdf
from flask import Flask, render_template, request, flash, redirect, send_file

# Import der Funktion zum Zusammensetzen (eigenes script dupl.py im selben Ordner)
from dupl import pdfduplex

app = Flask(__name__)
# Zum Identifizieren der Session, notwendig für flash()
# Zufälliger Byte-Folge wird mit os.urandom() erzeugt
app.secret_key = os.urandom(12)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        file_vs = request.files['vs']
        file_rs = request.files['rs']

        if file_vs.filename != '' and file_rs.filename != '':
            pdf_file_vs = pdf.PdfFileReader(file_vs)
            pdf_file_rs = pdf.PdfFileReader(file_rs)

            pdf_file_merged = pdfduplex(pdf_file_vs, pdf_file_rs)
            with open('/var/www/pdfduplex/merged.pdf', 'wb') as f:
                pdf_file_merged.write(f)

            return send_file('merged.pdf')

        else:
            flash ('Vorder- und Rückseite müssen hochgeladen werden')
            return redirect(request.url)
        return render_template('upload.html')
    else:
        return render_template('upload.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000, debug=True)
