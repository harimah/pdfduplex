#!/usr/bin/python3
# -*- coding: utf-8 -*-

import PyPDF2 as pdf
import sys
import os

# Check ob Command Line parameter stimmen

def pdfduplex (VS, RS):
    output= pdf.PdfFileWriter()

    # Checken ob Anzahl der Seiten übereinstimmt
    AnzahlVorderSeiten = VS.getNumPages()
    AnzahlRueckSeiten = RS.getNumPages()
    if AnzahlVorderSeiten != AnzahlRueckSeiten:
        print('Anzahl Seiten Vorder- und Rückseiten unterschiedlich, breche ab...')
        sys.exit(1)
    else:
        AnzahlSeiten = AnzahlVorderSeiten

    #Seiten umsortieren
    for i in range(0, AnzahlSeiten):
        Seite = VS.getPage(i)
        output.addPage(Seite)
        Seite = RS.getPage(AnzahlSeiten-1-i)
        output.addPage(Seite)
    return output

if __name__ =="__main__":
    if len(sys.argv) != 3:
        print ('Es müssen Vorder- und Rückseiten übergeben werden, breche ab...')
        sys.exit(1)

    # Existenz der Eingabedateien checken und öffnen
    try:
        VorderSeite = pdf.PdfFileReader(open(sys.argv[1], 'rb'))
    except:
        print (sys.argv[1], 'existiert nicht, breche ab...')
        sys.exit(1)

    try:
        RueckSeite = pdf.PdfFileReader(open(sys.argv[2], 'rb'))
    except:
        print (sys.argv[2], 'existiert nicht, breche ab...')
        sys.exit(1)

    # Ausgabedatei schreiben
    with open('new.pdf', 'wb') as f:
        output=pdfduplex(VS=VorderSeite, RS=RueckSeite)
        output.write(f)


# Eingangsdateien löschen
# os.remove(sys.argv[1])
# os.remove(sys.argv[2])

