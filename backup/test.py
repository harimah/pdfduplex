#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2018 harald <harald@rbserver>
#

import sys
import os
from flask import Flask

# Import der Funktion zum Zusammensetzen (eigenes script dupl.py im selben Ordner)

app = Flask(__name__)
# Zum Identifizieren der Session, notwendig für flash()
# Zufälliger Byte-Folge wird mit os.urandom() erzeugt

@app.route('/')
def index():
        return 'Hallo Harald'


